FROM python:3.7

RUN apt update -y && apt install -y ffmpeg

ENV  PYTHONUNBUFFERED 1

WORKDIR /app

RUN pip install --upgrade pip

COPY requirements.txt .

RUN pip install gtts

RUN pip install -r requirements.txt
